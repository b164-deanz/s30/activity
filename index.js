const express = require("express")

const mongoose = require("mongoose")


const app = express()

const port = 3001


mongoose.connect("mongodb+srv://deanrodel:deanrodel@clusterb164.wo1ny.mongodb.net/batch164_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
})

let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"))

db.once("open", () => console.log("We're connected to the cloud database"))



app.use(express.json())
app.use(express.urlencoded({extended: true}))


const taskSchema = new mongoose.Schema({
	name: String, 
	status: {
		type: String,
		default: "pending"
	}

});

const Task = mongoose.model("Task", taskSchema);





app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		} else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})
 
app.get("/tasks", (req, res) => {
		Task.find({}, (err, result) =>{
			if(err){
				return console.log(err)
			}else{
				return res.status(200).json({
					dataFromMDB: result
				})
			}
		})
})

const userSchema = new mongoose.Schema({
	
	userName: String,
	password: String
})

const UserTask = mongoose.model("userTask", userSchema)

app.post("/signup", (req, res) => {
	UserTask.findOne({userName: req.body.userName}, (err, result) =>{
		if(result !== '' && result.userName == req.body.userName){
			return res.send("You entered an already registered")
		}else{
			let users = new userTask({
				userName: req.body.userName,
				password: req.body.password
			})

			users.save((errSave, errResult) =>{
				if(errSave){
					return console.error(errSave)
				}else{
					return res.status(201).send("New User created")
				}
			})
		}
	})
})

app.get("/users", (req, res) => {
	UserTask.find({}, (err, result) => {
		if(err){
			console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
							
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`))


